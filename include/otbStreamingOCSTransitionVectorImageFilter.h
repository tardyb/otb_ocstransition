#ifndef __otbStreamingOCSTransitionVectorImageFilter_h_
#define __otbStreamingOCSTransitionVectorImageFilter_h_
#include "otbPersistentImageFilter.h" // Pas besoin de modif
#include "otbPersistentFilterStreamingDecorator.h" // Pas besoin de modif
#include "itkSimpleDataObjectDecorator.h"
#include "itkImageRegionSplitter.h"
#include "itkVariableSizeMatrix.h"
#include "itkVariableLengthVector.h"
#include <boost/functional/hash.hpp>
#include <unordered_map>
#include <string>
namespace otb{

template <class TInputImage, class TPrecision>
class ITK_EXPORT PersistentStreamingOCSTransitionVectorImageFilter :
    public otb::PersistentImageFilter<TInputImage, TInputImage>
{
public:
  /** Standard Self typedef */
  typedef PersistentStreamingOCSTransitionVectorImageFilter  Self;
  typedef otb::PersistentImageFilter<TInputImage, TInputImage> Superclass;
  typedef itk::SmartPointer<Self>                         Pointer;
  typedef itk::SmartPointer<const Self>                   ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Runtime information support. */
  itkTypeMacro(PersistentStreamingOCSTransitionVectorImageFilter, PersistentImageFilter);
  
  /** Image related typedefs. */
  typedef TInputImage                           ImageType;
  typedef typename ImageType::Pointer           InputImagePointer;
  typedef typename ImageType::RegionType        RegionType;
  typedef typename ImageType::SizeType          SizeType;
  typedef typename ImageType::IndexType         IndexType;
  typedef typename ImageType::PixelType         PixelType;
  typedef typename ImageType::InternalPixelType InternalPixelType;
  typedef TPrecision                            PrecisionType;
  typedef PrecisionType                         RealType;

  /** Image related typedefs. */
  itkStaticConstMacro(ImageDimension, unsigned int, TInputImage::ImageDimension);
  /** Smart Pointer type to a DataObject. */
  typedef typename itk::DataObject::Pointer DataObjectPointer;
  typedef itk::ProcessObject::DataObjectPointerArraySizeType DataObjectPointerArraySizeType;
  /** Type to use for computations. */
  typedef std::unordered_map<std::pair<std::string,PrecisionType>,PrecisionType,
			     boost::hash<std::pair<std::string,PrecisionType> > > 
                             MapOfKeyType;
  typedef typename MapOfKeyType::const_iterator MapOfKeyTypeIt;
  
  //typedef itk::VariableSizeMatrix<PrecisionType>        MatrixType;
  typedef itk::VariableLengthVector<PrecisionType>      RealPixelType;
  /** Type of DataObjects used for outputs */
  typedef itk::SimpleDataObjectDecorator<RealType>      RealObjectType;
  typedef itk::SimpleDataObjectDecorator<IndexType>     IndexObjectType;
  typedef itk::SimpleDataObjectDecorator<PixelType>     PixelObjectType;
  typedef itk::SimpleDataObjectDecorator<RealPixelType> RealPixelObjectType;
  //typedef itk::SimpleDataObjectDecorator<MatrixType>    MatrixObjectType;

  typedef itk::SimpleDataObjectDecorator<MapOfKeyType> MapOfKeyObjectType;

  /** Return the computed transitions map */
  MapOfKeyType GetMap() const
  {
    return this->GetMapOutput()->Get();
  }
  MapOfKeyObjectType* GetMapOutput();
  const MapOfKeyObjectType* GetMapOutput() const;
  /** Make a DataObject of the correct type to be used as the specified
   * output.
   */
  virtual DataObjectPointer MakeOutput(DataObjectPointerArraySizeType idx);
  using Superclass::MakeOutput;
  virtual void Reset(void);
  virtual void Synthetize(void);
  itkSetMacro(IgnoreInfiniteValues, bool);
  itkGetMacro(IgnoreInfiniteValues, bool);

  itkSetMacro(IgnoreUserDefinedValue, bool);
  itkGetMacro(IgnoreUserDefinedValue, bool);
  
  itkSetMacro(UserIgnoredValue, InternalPixelType);
  itkGetMacro(UserIgnoredValue, InternalPixelType);

  itkSetMacro(NumberOfClasses, unsigned int);
  itkGetMacro(NumberOfClasses, unsigned int);

  //itkSetMacro(StringKey, std::vector<std::string>);
  itkGetMacro(StringKey, std::vector<std::string>);

protected:
  PersistentStreamingOCSTransitionVectorImageFilter();

  virtual ~PersistentStreamingOCSTransitionVectorImageFilter() {}

  /** Pass the input through unmodified. Do this by Grafting in the
   *  AllocateOutputs method.
   */
  virtual void AllocateOutputs();

  virtual void GenerateOutputInformation();

  // virtual void PrintSelf(std::ostream& os, itk::Indent indent) const
  // {
  // }

  /** Multi-thread version GenerateData. */
  void  ThreadedGenerateData(const RegionType& outputRegionForThread, itk::ThreadIdType threadId); 
 
private:
 
  PersistentStreamingOCSTransitionVectorImageFilter(const Self &); //purposely not implemented
  void operator =(const Self&); //purposely not implemented

  /* Ignored values */
  bool m_IgnoreInfiniteValues;
  bool m_IgnoreUserDefinedValue;
  InternalPixelType m_UserIgnoredValue;
  std::vector<unsigned int>  m_IgnoredInfinitePixelCount;
  std::vector<unsigned int>  m_IgnoredUserPixelCount;

  /* Label to indices */
  unsigned int m_NumberOfClasses;

  /*Struct for count*/
  std::vector<std::string> m_StringKey;
  std::vector<RealType>   m_ThreadMapComponentAccumulators;
  //std::vector<MatrixType> m_ThreadMatrixAccumulators;
  std::vector<MapOfKeyType> m_ThreadMapAccumulators;
};//end class PersistentStreamingTransitionVectorImageFilter

/*=======================================================================


=======================================================================*/

template<class TInputImage, class TPrecision = typename itk::NumericTraits<typename TInputImage::InternalPixelType>::RealType>
class ITK_EXPORT StreamingOCSTransitionVectorImageFilter :
    public otb::PersistentFilterStreamingDecorator<PersistentStreamingOCSTransitionVectorImageFilter<TInputImage, TPrecision> >
{
  
public:
  /** Standard Self typedef */
  typedef StreamingOCSTransitionVectorImageFilter Self;
  typedef otb::PersistentFilterStreamingDecorator
  <PersistentStreamingOCSTransitionVectorImageFilter<TInputImage, TPrecision> > Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Type macro */
  itkNewMacro(Self);

  /** Creation through object factory macro */
  itkTypeMacro(StreamingOCSTransitionVectorImageFilter, PersistentFilterStreamingDecorator);

  typedef TInputImage                                 InputImageType;
  typedef typename Superclass::FilterType             StatFilterType;

  /** Type of DataObjects used for outputs */
  typedef typename StatFilterType::PixelType           PixelType;
  typedef typename StatFilterType::RealType            RealType;
  typedef typename StatFilterType::RealObjectType      RealObjectType;
  typedef typename StatFilterType::RealPixelType       RealPixelType;
  typedef typename StatFilterType::RealPixelObjectType RealPixelObjectType;

  typedef typename StatFilterType::MapOfKeyType        MapOfKeyType;
  typedef typename StatFilterType::MapOfKeyObjectType  MapOfKeyObjectType;
  typedef typename StatFilterType::InternalPixelType   InternalPixelType;

  using Superclass::SetInput;
  void SetInput(InputImageType * input)
  {
    this->GetFilter()->SetInput(input);
  }
  const InputImageType * GetInput()
  {
    return this->GetFilter()->GetInput();
  }

  /** Return the computed Matrix */
  MapOfKeyType GetMap() const
  {
    return this->GetFilter()->GetMap();
  }
  MapOfKeyObjectType* GetMapOutput()
  {
    return this->GetFilter()->GetMapOutput();
  }
  const MapOfKeyObjectType* GetMapOutput() const
  {
    return this->GetFilter()->GetMapOutput();
  }
  void SetLabelToIndices(std::vector<unsigned int> label)
  {
    return this->GetFilter()->SetLabelToIndices(label);
  }
  
  std::vector<std::string> GetStringKey()
  {
    return this->GetFilter()->GetStringKey();
  }
  otbSetObjectMemberMacro(Filter, NumberOfClasses, unsigned int);
  otbGetObjectMemberMacro(Filter, NumberOfClasses, unsigned int);

  otbSetObjectMemberMacro(Filter, IgnoreInfiniteValues, bool);
  otbGetObjectMemberMacro(Filter, IgnoreInfiniteValues, bool);

  otbSetObjectMemberMacro(Filter, IgnoreUserDefinedValue, bool);
  otbGetObjectMemberMacro(Filter, IgnoreUserDefinedValue, bool);

  otbSetObjectMemberMacro(Filter, UserIgnoredValue, InternalPixelType);
  otbGetObjectMemberMacro(Filter, UserIgnoredValue, InternalPixelType);
  
protected:
  /** Constructor */
  StreamingOCSTransitionVectorImageFilter() {}

  /** Destructor */
  virtual ~StreamingOCSTransitionVectorImageFilter() {}

private:
  StreamingOCSTransitionVectorImageFilter(const Self &); //purposely not implemented
  void operator =(const Self&); //purposely not implemented

  //virtual void PrintSelf(std::ostream& os, itk::Indent indent) const
  //{
  // }

};//end of class StreamingTransitionVectorImageFilter

}//end namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbStreamingOCSTransitionVectorImageFilter.hxx"
#endif

#endif
