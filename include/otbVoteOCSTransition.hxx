#ifndef __otbVoteTransition_hxx
#define __otbVoteTransition_hxx

#include "otbVoteOCSTransition.h"
#include "itkImageRegionIterator.h"
#include "itkProgressReporter.h"

namespace otb
{
/**Constructor */
template <class TInputImage, class TOutputImage, class TFunctor>
BinaryFunctorTransitionImageFilterWithNBands<TInputImage, TOutputImage, TFunctor>
::BinaryFunctorTransitionImageFilterWithNBands()
{
  this->SetNthOutput(0,TOutputImage::New());
}

/** Threading*/
template <class TInputImage ,class TOutputImage, class TFunctor>
void BinaryFunctorTransitionImageFilterWithNBands<TInputImage, TOutputImage, TFunctor >
::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, itk::ThreadIdType threadId)
{

  /** Inputs pointers */
  InputImageConstPointerType inputPtr = this->GetInput(0);
  InputImageConstPointerType inputPtrRef = this->GetInput(1);
  OutputImagePointerType outputPtr = this->GetOutput(0);

  // Progress Reporting 
  itk::ProgressReporter progress(this, threadId, outputRegionForThread.GetNumberOfPixels());

  /**Define Iterators */
  typedef itk::ImageRegionConstIterator<InputImageType> InputIteratorType;
  typedef itk::ImageRegionIterator<OutputImageType>     OutputIteratorType;

  InputIteratorType inIt(inputPtr, outputRegionForThread);
  InputIteratorType refIt(inputPtrRef, outputRegionForThread);
  OutputIteratorType outIt(outputPtr, outputRegionForThread);

  for(inIt.GoToBegin(),refIt.GoToBegin(),outIt.GoToBegin(); !inIt.IsAtEnd() && !refIt.IsAtEnd() && !outIt.IsAtEnd(); ++inIt,++refIt,++outIt)
    {
      PixelType result = m_Functor(inIt.Get(),refIt.Get(),m_MapOfKey,m_VectorLabel, m_FlagGuide,m_FlagGuideN,m_Margin,m_HVotersMarge);
      outIt.Set(result);
    }

}//end threaded
 
}//end otb

#endif
