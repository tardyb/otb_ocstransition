#ifndef _otbVoteOCSTransition_h_
#define _otbVoteOCSTransition_h_

#include "itkImageToImageFilter.h"
#include "itkVariableLengthVector.h"
#include "itkVariableSizeMatrix.h"
#include <unordered_map>
#include <boost/functional/hash.hpp>

namespace otb
{
template <typename KeyType, typename ValuesType>
  struct Compare
    {
      typedef typename std::pair<KeyType,ValuesType> PairType;
      bool operator()(PairType x,PairType y)
      {
	return x.second>y.second;
      }
    };


  template <typename PixelType>
  class TransitionMajorityFunctor
  {
  public:
    
    using ValueType = typename PixelType::ValueType;
    using VectorType = typename std::vector<ValueType>;
    using MapType = typename std::map<ValueType, ValueType>;
    using MapTypeIt = typename MapType::const_iterator;
    using LabelVectorType = typename std::vector<unsigned int>;

    using MapOfKeysType = typename std::unordered_map<std::pair<std::string, unsigned int> ,double, boost::hash<std::pair<std::string,unsigned int> > >;
    TransitionMajorityFunctor() = default;

    PixelType operator()(PixelType voters,PixelType ref, MapOfKeysType &mapOfKey, LabelVectorType vectorLabel, bool flag_guide, bool flag_guideN, unsigned int tolerance,unsigned int marge)
    {
      auto nbVoters = voters.GetSize();
      MapType VoteMap;
      for(size_t i =0; i<nbVoters;i++)
	{
	  if(VoteMap.find(voters[i]) != VoteMap.end())
    	    {
    	      VoteMap[voters[i]] += 1;
    	    }
    	  else
    	    {
    	      VoteMap[voters[i]] = 1;
    	    }
	} // end count pixel
      //Initialize output pixel
      PixelType labeled{1};
      labeled.Fill(0);
      if (VoteMap.size() < 2)
	{
	  // if only one class, return the label and the confiance
	  labeled[0] = VoteMap.begin()->first;
	}
      else
	{
	  labeled = DoSelection(voters,ref,mapOfKey,VoteMap,tolerance,flag_guide,flag_guideN,vectorLabel,marge);
	}
      return labeled;
    }
    
    PixelType DoSelection(PixelType voters,PixelType ref, MapOfKeysType &mapOfKey,MapType &VoteMap,unsigned int tolerance, bool flag_guide, bool flag_guideN, LabelVectorType &vectorLabel,unsigned int marge)
    {
      //Initialize output pixel
      PixelType labeled{1};
      labeled.Fill(0);
      // Convert Map in vector for sorting
      std::vector<std::pair<ValueType,ValueType>> VectorVote(VoteMap.size());
      std::copy(VoteMap.begin(),VoteMap.end(),VectorVote.begin());
      std::sort(VectorVote.begin(),VectorVote.end(),Compare<ValueType,ValueType>());
      // Define if equality with margin
      unsigned int nbVoters = voters.GetSize();
      float margin = (100.0/nbVoters)*tolerance;
      margin ++; //remove warning temp
      float v1 = (100.0*VectorVote[0].second)/nbVoters;
      float v2 = (100.0*VectorVote[1].second)/nbVoters;
      if ((v1-v2) !=0) //(v1>v2+margin)
	{
	  //Able to take a decision
	  labeled[0] = VectorVote[0].first;
	}
      else
	{
	  //Define the candidates
	  LabelVectorType candidates;
	  if (flag_guide)
	    {
	      //std::cout <<"Enter voter";
	      if (flag_guideN)
		{
		  unsigned int i=1;
		  while(v1> ((100.0*VectorVote[i].second)/nbVoters)+marge)
		    {
		      candidates.push_back(VectorVote[i].first);
		      i++;
		    }
		}
	      else
		{
		  
		  for(size_t i=0;i<VectorVote.size();i++)
		    {
		      //std::cout << "Candidats : " << VectorVote[i].first << std::endl;
		      candidates.push_back(VectorVote[i].first);
		    }
		}	    
	    }
	  else
	    {
	      //std::cout << "Before copy";
	      //default transition max
	      candidates=vectorLabel;
	      //std::cout << "End of Copy" << candidates[0]<< std::endl;
	    }
	  //Create reference key
	  std::string refKey = "";
	  for(size_t r=0;r<ref.Size();++r)
	    {
	      std::string refVal = std::to_string((unsigned int) ref[r]);
	      for (size_t s = refVal.size();s<3;++s)
		refKey+='0';
	      refKey+= refVal;
	    }
	  //Evaluate transition
	  double currentMax = 0;
	  unsigned int classLabel = 0;
	  // Choix par maximum
	  for(size_t t=0;t<candidates.size();++t)
	    {
	      std::pair<std::string,unsigned int> Key = std::make_pair(refKey,candidates[t]);
	      if (mapOfKey.find(Key)!=mapOfKey.end())
		{
		  double currentValue = mapOfKey[Key];
		  if (currentMax < currentValue)
		    {
		      currentMax = currentValue;
		      classLabel = candidates[t];
		    }
		}
	    }

	  labeled[0] = classLabel;
	}
      return labeled;
    } //end of function doSelection
     
  };//end of class functor
  
  /** Filter */
  template <class TInputImage, class TOutputImage, class TFunctor>
  class ITK_EXPORT BinaryFunctorTransitionImageFilterWithNBands :
    public itk::ImageToImageFilter <TInputImage, TOutputImage>
  {
  public:
    typedef BinaryFunctorTransitionImageFilterWithNBands Self;
    typedef itk::ImageToImageFilter<TInputImage, TOutputImage> Superclass;
    typedef itk::SmartPointer<Self>       Pointer;
    typedef itk::SmartPointer<const Self> ConstPointer;
    typedef TFunctor FunctorType;

    /** Method for creation through the object factory. */
    itkNewMacro(Self);
    /** Macro defining the type*/
    itkTypeMacro(BinaryFunctorTransitionImageFilterWithNBands, SuperClass);

    /** Pointer for threading */
    typedef TInputImage                                InputImageType;
    typedef typename InputImageType::ConstPointer      InputImageConstPointerType;
    typedef typename InputImageType::InternalPixelType ValueType;
    typedef typename InputImageType::PixelType         PixelType;
  
    typedef TOutputImage                               OutputImageType;
    typedef typename OutputImageType::Pointer          OutputImagePointerType;
    typedef typename OutputImageType::RegionType       OutputImageRegionType;
    typedef typename OutputImageType::PixelType        LabelType;  

    using MatrixType = typename itk::VariableSizeMatrix<double>;
    using LabelToIndicesType = typename std::unordered_map<unsigned int , unsigned int>;
    using KeyToIndicesType = typename std::unordered_map<std::string, unsigned int>;
    using LabelVectorType = typename  std::vector<unsigned int>;

    using MapOfKeysType = typename std::unordered_map<std::pair<std::string, unsigned int> ,double, boost::hash<std::pair<std::string,unsigned int> > >;
    /** ITK MACRO */
    itkSetMacro(TransitionMatrix, MatrixType);
    itkGetMacro(TransitionMatrix, MatrixType);
    
    itkSetMacro(FlagGuide, bool);
    itkGetMacro(FlagGuide, bool);
    
    itkSetMacro(FlagGuideN, bool);
    itkGetMacro(FlagGuideN, bool);
    
    itkSetMacro(HVotersMarge, unsigned int);
    itkGetMacro(HVotersMarge, unsigned int);

    itkSetMacro(Margin, unsigned int);
    itkGetMacro(Margin, unsigned int);
    void SetLabelToIndices(LabelToIndicesType mapLabels)
    {
      m_LabelToIndices = mapLabels;
    }

    void SetKeyToIndices(KeyToIndicesType mapKeys)
    {
      m_KeyToIndices = mapKeys;      
    }
    void SetVectorLabel(LabelVectorType vectLab)
    {
      m_VectorLabel = vectLab;
    }
    void SetMapOfKey(MapOfKeysType mapOfKey)
    {
      m_MapOfKey = mapOfKey;
    }
  protected:
    /** Constructor */
    BinaryFunctorTransitionImageFilterWithNBands();
    /** Destructor */
    virtual ~BinaryFunctorTransitionImageFilterWithNBands() {}
  
    /** Threaded generate data */
    virtual void ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, itk::ThreadIdType threadId);
  
    void GenerateOutputInformation()
    {
      Superclass::GenerateOutputInformation();
      // Define the number of output bands
      this->GetOutput()->SetNumberOfComponentsPerPixel(1);
    }

    /**PrintSelf method */
    //void PrintSelf(std::ostream& os, itk::Indent indent) const override;

  private:
    BinaryFunctorTransitionImageFilterWithNBands(const Self &); //purposely not implemented
    void operator =(const Self&); //purposely not implemented

    MatrixType m_TransitionMatrix;
    LabelToIndicesType  m_LabelToIndices;
    KeyToIndicesType m_KeyToIndices;
    LabelVectorType  m_VectorLabel;
    MapOfKeysType m_MapOfKey;
    bool m_FlagGuide;
    bool m_FlagGuideN;
    unsigned int m_Margin;
    unsigned int m_HVotersMarge;
    TFunctor m_Functor;
  };// end of filter
}//end namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbVoteOCSTransition.hxx"
#endif
#endif
