#ifndef __otbStreamingOCSTransitionVectorImageFilter_hxx
#define __otbStreamingOCSTransitionVectorImageFilter_hxx
#include "otbStreamingOCSTransitionVectorImageFilter.h"

#include "itkImageRegionIterator.h"
#include "itkImageRegionConstIteratorWithIndex.h"
#include "itkProgressReporter.h"
#include "otbMacro.h"
#include <string>
namespace otb{

/**test before merge branch */
template<class TInputImage, class TPrecision>
PersistentStreamingOCSTransitionVectorImageFilter<TInputImage, TPrecision>
::PersistentStreamingOCSTransitionVectorImageFilter()
  : m_IgnoreInfiniteValues(true),
    m_IgnoreUserDefinedValue(false)
{
  this->itk::ProcessObject::SetNthOutput(1,this->MakeOutput(1).GetPointer());

  // Initiate ignored pixel counters
  m_IgnoredInfinitePixelCount= std::vector<unsigned int>(this->GetNumberOfThreads(), 0);
  m_IgnoredUserPixelCount= std::vector<unsigned int>(this->GetNumberOfThreads(), 0);

}

template<class TInputImage, class TPrecision>
typename PersistentStreamingOCSTransitionVectorImageFilter<TInputImage,TPrecision>::MapOfKeyObjectType*
PersistentStreamingOCSTransitionVectorImageFilter<TInputImage,TPrecision>
::GetMapOutput()
{
  //std::cout << "getouttemp"<<std::endl;
  return static_cast<MapOfKeyObjectType*>(this->itk::ProcessObject::GetOutput(1));
}
template<class TInputImage, class TPrecision>
const typename PersistentStreamingOCSTransitionVectorImageFilter<TInputImage,TPrecision>::MapOfKeyObjectType*
PersistentStreamingOCSTransitionVectorImageFilter<TInputImage,TPrecision>
::GetMapOutput() const
{
  return static_cast<const MapOfKeyObjectType*>(this->itk::ProcessObject::GetOutput(1));
}


template<class TInputImage, class TPrecision>
itk::DataObject::Pointer
PersistentStreamingOCSTransitionVectorImageFilter<TInputImage, TPrecision>
::MakeOutput(DataObjectPointerArraySizeType output)
{
  switch(output)
    {
    case 0:
      return static_cast<itk::DataObject*>(TInputImage::New().GetPointer());
      break;      
    case 1:
      return static_cast<itk::DataObject*>(MapOfKeyObjectType::New().GetPointer());
      break;
    default:
      return static_cast<itk::DataObject*>(TInputImage::New().GetPointer());
      break;
    }
}

template<class TInputImage, class TPrecision>
void
PersistentStreamingOCSTransitionVectorImageFilter<TInputImage, TPrecision>
::GenerateOutputInformation()
{
  Superclass::GenerateOutputInformation();
  if (this->GetInput())
    {
    this->GetOutput()->CopyInformation(this->GetInput());
    this->GetOutput()->SetLargestPossibleRegion(this->GetInput()->GetLargestPossibleRegion());
    if (this->GetOutput()->GetRequestedRegion().GetNumberOfPixels() == 0)
      {
      this->GetOutput()->SetRequestedRegion(this->GetOutput()->GetLargestPossibleRegion());
      }
    }
}

template<class TInputImage, class TPrecision>
void
PersistentStreamingOCSTransitionVectorImageFilter<TInputImage, TPrecision>
::AllocateOutputs()
{
  // This is commented to prevent the streaming of the whole image for the first stream strip
  // It shall not cause any problem because the output image of this filter is not intended to be used.
  //InputImagePointer image = const_cast< TInputImage * >( this->GetInput() );
  //this->GraftOutput( image );
  // Nothing that needs to be allocated for the remaining outputs
}

template<class TInputImage, class TPrecision>
void
PersistentStreamingOCSTransitionVectorImageFilter<TInputImage, TPrecision>
::Reset()
{
  TInputImage * inputPtr = const_cast<TInputImage *>(this->GetInput());
  inputPtr->UpdateOutputInformation();

  unsigned int numberOfThreads = this->GetNumberOfThreads();
  //unsigned int numberOfComponent = inputPtr->GetNumberOfComponentsPerPixel();
  m_ThreadMapComponentAccumulators.resize(numberOfThreads);
  std::fill(m_ThreadMapComponentAccumulators.begin(), m_ThreadMapComponentAccumulators.end(), itk::NumericTraits<PrecisionType>::Zero);
  
  //  Reset Map
  MapOfKeyType emptyMap;
  emptyMap.clear();
  m_ThreadMapAccumulators.resize(numberOfThreads);
  this->GetMapOutput()->Set(emptyMap);
  std::fill(m_ThreadMapAccumulators.begin(), m_ThreadMapAccumulators.end(), emptyMap);
  
  if (m_IgnoreInfiniteValues)
    {
      m_IgnoredInfinitePixelCount= std::vector<unsigned int>(numberOfThreads, 0);
    }

  if (m_IgnoreUserDefinedValue)
    {
    m_IgnoredUserPixelCount= std::vector<unsigned int>(this->GetNumberOfThreads(), 0);
    }
}

template<class TInputImage, class TPrecision>
void
PersistentStreamingOCSTransitionVectorImageFilter<TInputImage, TPrecision>
::Synthetize()
{

  //std::cout << "Synthetize begin" << std::endl;
  //TInputImage * inputPtr = const_cast<TInputImage *>(this->GetInput());
  //const unsigned int nbPixels = inputPtr->GetLargestPossibleRegion().GetNumberOfPixels(); //utile?
  //const unsigned int numberOfComponent = inputPtr->GetNumberOfComponentsPerPixel();// inutile
  
  MapOfKeyType streamMapAccumulator;
  //Ensure the map is empty
  streamMapAccumulator.clear();
  
  RealType streamMapComponentAccumulator = itk::NumericTraits<RealType>::Zero;

  //unsigned int ignoredInfinitePixelCount = 0;
  //unsigned int ignoredUserPixelCount = 0;

  // Accumulate results from all threads
  const itk::ThreadIdType numberOfThreads = this->GetNumberOfThreads();
  for(itk::ThreadIdType threadId = 0; threadId < numberOfThreads; ++threadId)
    {
      MapOfKeyType currMap = m_ThreadMapAccumulators[threadId];
      
      for (MapOfKeyTypeIt it = currMap.begin(); it != currMap.end();it++)
	{
	  std::pair<std::string,unsigned int> key = it->first;
	  if (streamMapAccumulator.find(key) != streamMapAccumulator.end())
	    {
	      streamMapAccumulator[key] += it->second;
	    }
	  else
	    {
	      streamMapAccumulator[key] = it->second;
	    }
	}
      streamMapComponentAccumulator += m_ThreadMapComponentAccumulators[threadId];
    }

  // Normalize the map
  for (MapOfKeyTypeIt it = streamMapAccumulator.begin(); it != streamMapAccumulator.end();it++)
    {
      streamMapAccumulator[it->first] = it->second/streamMapComponentAccumulator;
    }
  this->GetMapOutput()->Set(streamMapAccumulator);
  
}

template<class TInputImage, class TPrecision>
void
PersistentStreamingOCSTransitionVectorImageFilter<TInputImage, TPrecision>
::ThreadedGenerateData(const RegionType& outputRegionForThread, itk::ThreadIdType threadId)
{
  // Support progress methods/callbacks
  itk::ProgressReporter progress(this, threadId, outputRegionForThread.GetNumberOfPixels());
  // Grab the input
  InputImagePointer inputPtr = const_cast<TInputImage *>(this->GetInput(0));
  itk::ImageRegionConstIteratorWithIndex<TInputImage> it(inputPtr, outputRegionForThread);
  for (it.GoToBegin(); !it.IsAtEnd(); ++it, progress.CompletedPixel())
    {
      const PixelType& vectorValue = it.Get();
      float finiteProbe = 0.;
      bool userProbe = true;
      for (unsigned int j = 0; j < vectorValue.GetSize(); ++j)
	{
	  finiteProbe += (float)(vectorValue[j]);
	  userProbe = userProbe && (vectorValue[j] == m_UserIgnoredValue);
	}
      if (m_IgnoreInfiniteValues && !(vnl_math_isfinite(finiteProbe)))
	{
	  m_IgnoredInfinitePixelCount[threadId] ++;
	}
      else
	{
	  if (m_IgnoreUserDefinedValue && (userProbe))
	    {
	      m_IgnoredUserPixelCount[threadId] ++;
	    }
	  else
	    {
	      MapOfKeyType& threadMap = m_ThreadMapAccumulators[threadId];
	      RealType& threadMapComponent = m_ThreadMapComponentAccumulators[threadId];
	      unsigned int sizeVector = vectorValue.GetSize();

	      // Create a key
	      std::string key = "";
	      
	      for (unsigned int i =0; i<sizeVector-1; ++i)
		{
		  std::string value = std::to_string((unsigned int) vectorValue[i]);
		  for(size_t s =value.size(); s< 3;s++)
		    {
		      key += "0";
		    }
		  key += value;
		}
	      unsigned int transitionLabel = (unsigned int) vectorValue[sizeVector];
	      std::pair<std::string,unsigned int> pixelKey = std::make_pair(key,transitionLabel);
	      if (threadMap.find(pixelKey)!= threadMap.end())
		{
		  threadMap[pixelKey] += 1;
		}
	      else
		{
		  threadMap[pixelKey] = 1;
		}
	      threadMapComponent++;
	    }
	}
    }
}

}//end namespace otb

#endif
