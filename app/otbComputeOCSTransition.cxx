
#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbImageListToVectorImageFilter.h"
#include "otbMultiToMonoChannelExtractROI.h"
#include "otbImageList.h"
#include <sstream>
#include <algorithm> 
#include <boost/algorithm/string.hpp>

#include "otbStreamingOCSTransitionVectorImageFilter.h" //filtre a modif
namespace otb
{
namespace Wrapper
{

  class ComputeOCSTransition : public otb::Wrapper::Application
{
public:
  /** Standard class typedefs. */
  typedef ComputeOCSTransition Self;
  typedef Application Superclass;
  typedef itk::SmartPointer<Self> Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;


  /** Filters typedef for concatenation */
  typedef otb::ImageList<FloatImageType> ImageListType;
  typedef ImageListToVectorImageFilter<ImageListType,
  				       FloatVectorImageType> ListConcatenerFilterType;

  typedef MultiToMonoChannelExtractROI<FloatVectorImageType::InternalPixelType,
                                       FloatImageType::PixelType>               ExtractROIFilterType;
  typedef ObjectList<ExtractROIFilterType>                                      ExtractROIFilterListType;
  
  /** Standard macro */
  itkNewMacro(Self);

  itkTypeMacro(ComputeOCSTransition, otb::Wrapper::Application);

  using FilterType = otb::StreamingOCSTransitionVectorImageFilter<FloatVectorImageType>;
  typedef FilterType::MapOfKeyType MapOfKeyType;
private:
  void DoInit() 
  {
    SetName("ComputeOCSTransition");
    SetDescription("Computes global transition probability between labels from different land cover map.");
    SetDocLongDescription("Compute global transition probability");
      
    AddParameter(ParameterType_InputImageList, "il", "Input images");
    SetParameterDescription( "il", "List of input images filenames. Requires at least two images." );

    AddParameter(ParameterType_Float, "bv", "Background Value");
    SetParameterDescription( "bv", "Background value to ignore in statistics computation." );
    MandatoryOff("bv");
    
    AddParameter(ParameterType_String,"nomen","Nomenclature csv file");
    SetParameterDescription("nomen","Nomenclature csv file on class per line in format className:classValue");

    AddParameter(ParameterType_OutputFilename, "out", "Output csv file");
    SetParameterDescription( "out", "CSV filename where the transition probability are saved for future reuse." );
    MandatoryOff("out");

   // Doc example parameter settings
   SetDocExampleParameterValue("il", "QB_1_ortho.tif QB_2_ortho.tif");
   SetDocExampleParameterValue("out", "EstimateImageStatisticsQB1.csv");
  }

  void DoUpdateParameters()
  {
    // Nothing to do here : all parameters are independent
    m_Concatener = ListConcatenerFilterType::New();
    m_ImageList = ImageListType::New();
    m_ExtractorList = ExtractROIFilterListType::New();
  }

  void DoExecute()
   {
      
     FloatVectorImageListType::Pointer imageList = GetParameterImageList("il");
     if (imageList->Size() < 2)
       {
	 itkExceptionMacro(<< " ComputeTransition requires at least two input images.")
       }
    
     imageList->GetNthElement(0)->UpdateOutputInformation();
     m_ImageList = ImageListType::New();
     m_Concatener = ListConcatenerFilterType::New();
     
     for (unsigned int i=0; i<imageList->Size(); i++)
       {
     	 FloatVectorImageType::Pointer image = imageList->GetNthElement(i);
     	 image->UpdateOutputInformation();
     	 ExtractROIFilterType::Pointer converter = ExtractROIFilterType::New();
     	 converter->SetInput(image);
     	 converter->SetChannel(1);
	 m_ExtractorList->PushBack(converter);
     	 m_ImageList->PushBack(converter->GetOutput());
       }
     m_Concatener->SetInput(m_ImageList);
     // Convert labels in map to set the filter parameter
     std::string in_nomen_file{""};
     in_nomen_file = GetParameterString("nomen");
     std::ifstream csvFile(in_nomen_file);
     if(!csvFile)
       itkGenericExceptionMacro(<<"Could not open file"<<in_nomen_file<<"\n");

     // Construct the map
     std::vector<unsigned int> Labels;
     char delim = '\n';
     std::string ligne;
     unsigned int numLigne = 0;
     while(std::getline(csvFile,ligne,delim))
       {
	 Labels.resize(Labels.size()+1);
     	 std::vector<std::string> chaine;
     	 boost::split(chaine,ligne,boost::is_any_of(":"));
     	 Labels[numLigne] = stoi(chaine[1]);
     	 numLigne++;
	 
	 
       }
     csvFile.close();

     // Call filter
     m_transitionEstimator = FilterType::New();
     m_transitionEstimator->SetInput(m_Concatener->GetOutput());
     //m_transitionEstimator->SetLabelToIndices(Labels);
     m_transitionEstimator->SetNumberOfClasses(Labels.size());
     if(HasValue("bv")==true)
       {
     	 m_transitionEstimator->SetIgnoreUserDefinedValue(true);
     	 m_transitionEstimator->SetUserIgnoredValue(GetParameterFloat("bv"));
       }
     //std::cout << "Update call" << std::endl;
     m_transitionEstimator->Update();
     //std::cout << "Init" << std::endl;
     MapOfKeyType transitionMap = m_transitionEstimator->GetMap(); 
     std::vector<std::string> keys = m_transitionEstimator->GetStringKey();
     // Write Matrix in file
     std::ofstream outFile;
     outFile.open(this->GetParameterString("out").c_str());
     outFile<<std::fixed;
     outFile.precision(10);
  
     // Write all the key found during the processing
     // Initialize a vector with all keys to produce the matrix
     //Dynamic size of vector to remove duplicate string keys
     std::vector<std::string> listOfKeys;//(transitionMap.size());
     std::vector<std::string>::iterator itString;
     std::string myChaine= "#TransitionKey:";
     MapOfKeyType::const_iterator it = transitionMap.begin();
     
    
     bool flag = true;
     while (it != transitionMap.end())
       {
	 std::string KeyValue = it->first.first;
	 it++;
	 if (listOfKeys.empty())
	   {
	     listOfKeys.push_back(KeyValue);
	     myChaine +=KeyValue+",";
	   }
	 else
	   {
	     itString = std::find(listOfKeys.begin(),listOfKeys.end(),KeyValue);
	     if (itString == listOfKeys.end())
	       {
		 flag = true;
		 // If the key don't exist in the vector write the key in the file
		 // and push in the key vector
		 listOfKeys.push_back(KeyValue);
		 myChaine += KeyValue;
	       }
	     else
	       {
		 flag = false;
	       }
	     

	     if (it != transitionMap.end())
	       {
		 if (flag)
		   myChaine +=",";
	       }
	     else
	       {
		 myChaine.erase(myChaine.size()-1);
		 myChaine += "\n";
		 outFile << myChaine;
	       }
	   }
       }
     
     outFile<< "#TransitionLabel:";
     for(unsigned int i =0; i<Labels.size();i++)
       {
	 outFile<< Labels[i];
	 if (i+1 < Labels.size())
	   {
	     outFile<<",";
	   }
	 else
	   {
	     outFile<<"\n"; 
	   }

       }     
     
     // Write transition in the matrix

     for (unsigned int r = 0; r<listOfKeys.size(); r++)
       {
	 for (unsigned int c = 0; c< Labels.size();c++)
	   {
	     std::pair<std::string,unsigned int> key = std::make_pair(listOfKeys[r],Labels[c]); 
	     if (transitionMap.find(key) != transitionMap.end())
	       {
		 outFile << transitionMap[key];
	       }
	     else
	       {
		 outFile << 0;
	       }
	     
	     if (c < Labels.size()-1)
      	       {
      		 outFile << ",";
      	       }
     	     else
      	       {
      		 outFile << "\n";
      	       }
	   }
       }

     outFile.close();
     
   }
  
  ListConcatenerFilterType::Pointer  m_Concatener;
  ExtractROIFilterListType::Pointer  m_ExtractorList;
  ImageListType::Pointer        m_ImageList;
  FilterType::Pointer m_transitionEstimator;
  itk::LightObject::Pointer m_FilterRef;
};

}
}

OTB_APPLICATION_EXPORT(otb::Wrapper::ComputeOCSTransition)
