#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbImageListToVectorImageFilter.h"
#include "otbMultiToMonoChannelExtractROI.h"
#include "otbImageList.h"
#include <boost/functional/hash.hpp>
#include "otbVoteOCSTransition.h"
#include <itkVariableSizeMatrix.h>
#include <unordered_map>

namespace otb
{
namespace Wrapper
{

class VoteOCSTransition : public Application
{
public:

  /** Standard class typedefs. */
  typedef VoteOCSTransition Self;
  typedef Application Superclass;

  typedef itk::SmartPointer<Self> Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Filters typedef for concatenation */
  typedef otb::ImageList<FloatImageType> ImageListType;
  typedef ImageListToVectorImageFilter<ImageListType,
  				       FloatVectorImageType> ListConcatenerFilterType;
  
  typedef MultiToMonoChannelExtractROI<FloatVectorImageType::InternalPixelType,
                                       FloatImageType::PixelType>               ExtractROIFilterType;
  typedef ObjectList<ExtractROIFilterType>                                      ExtractROIFilterListType;

  typedef itk::VariableSizeMatrix<double> MatrixType;
  typedef std::unordered_map<unsigned int, unsigned int> LabelToIndicesType;
  typedef std::unordered_map<std::string,unsigned int> KeyToIndicesType;
  typedef std::vector<unsigned int> LabelVectorType;
  typedef std::unordered_map<std::pair<std::string,unsigned int>,double,
			     boost::hash<std::pair<std::string,unsigned int> > > 
                             MapOfKeysType;
  /** Standard macro */
  //
  itkNewMacro(Self);
  itkTypeMacro(VoteOCSTransition, otb::Wrapper::Application);
  using FunctorType = otb::TransitionMajorityFunctor<FloatVectorImageType::PixelType>;

  using FilterType = otb::BinaryFunctorTransitionImageFilterWithNBands<FloatVectorImageType,
								FloatVectorImageType,
								FunctorType>;;

private:

  void DoInit()
  {

    SetName("VoteOCSTransition");
    SetDescription("This application use supervised maps, transition land cover classes and voters to produce a land cover map");

    AddParameter(ParameterType_InputImageList, "il","Input classifications");
    SetParameterDescription("il","Input votings");

    AddParameter(ParameterType_InputImageList, "ref","Reference classifications");
    SetParameterDescription("ref","Input past supervised classification");
    
    AddParameter(ParameterType_String,"tr","Transition matrix file");
    SetParameterDescription("tr","Input transition matrix, from ComputeOCSTransition app");

    AddParameter(ParameterType_OutputImage,"out","The output fused image");
    SetParameterDescription("out","The output map");

    AddParameter(ParameterType_Choice,"mode","Transition evaluation mode");
    SetParameterDescription("mode","Transition evaluation mode");
    
    AddChoice("mode.classes","All classes are considered");
    SetParameterDescription("mode.classes","All classes are considered in case of tie in vote");

    AddChoice("mode.voters","All voters considered");
    SetParameterDescription("mode.voters","All voters are considered in case of tie in vote");
    
    AddChoice("mode.hvoters","Only high voters are considered");
    SetParameterDescription("mode.hvoters","Only high voters are considered");
    // Default mode classes
    SetParameterString("mode","classes",false);

    AddParameter(ParameterType_Int,"margin","Define the margin in term of number of voters");
    SetParameterDescription("margin","Define the margin in term of number of voters. If the margin is greater than the number of voters, the margin is set to 0");
    SetDefaultParameterInt("margin",0);
    MandatoryOff("margin");
      
      
  }

  void DoUpdateParameters()
  {
    // Nothing to do here : all parameters are independent
    m_Concatener = ListConcatenerFilterType::New();
    m_ImageList = ImageListType::New();
    m_ExtractorList = ExtractROIFilterListType::New();
  }

  int CSVTransitionMatrixFileReader(const std::string fileName, MapOfKeysType& mapOfKey,LabelVectorType& vectorLabels)
  {
    std::ifstream inFile;
    inFile.open(fileName.c_str());
    if(!inFile)
      {
	std::cerr << "Transition Matrix File opening problem with file:" << std::endl;
 	std::cerr << fileName.c_str() << std::endl;
 	return EXIT_FAILURE;
      }
    else
      {
	std::string currentLine, refLabelsLine, transLabelsLine, currentValue;
 	const char endCommentChar = ':';
 	const char separatorChar = ',';
 	const char eolChar = '\n';
 	std::getline(inFile, refLabelsLine, endCommentChar); // Skips the comments
 	std::getline(inFile, refLabelsLine, eolChar); // Gets the first line after the comment char until the End Of Line char
 	std::getline(inFile, transLabelsLine, endCommentChar); // Skips the comments
 	std::getline(inFile, transLabelsLine, eolChar); // Gets the second line after the comment char until the End Of Line char
	
 	std::istringstream issRefLabelsLine(refLabelsLine);
 	std::istringstream issTransLabelsLine(transLabelsLine);
 	//int itLab = 0;
	
	// In the file the transition key are unique
	std::vector<std::string> vectorOfKey;
	while (issRefLabelsLine.good())
	  {
	    std::getline(issRefLabelsLine,currentValue,separatorChar);
	    std::string value = currentValue.c_str();
	    vectorOfKey.push_back(value);
	  }
	
	while (issTransLabelsLine.good())
	  {
	    std::getline(issTransLabelsLine,currentValue,separatorChar);
	    unsigned int value = std::atoi(currentValue.c_str());
	    vectorLabels.push_back(value);
	  }

	unsigned int nbLabels = vectorLabels.size();
	unsigned int nbKeys = vectorOfKey.size();
	
	//reading the file
	for(unsigned int itRow = 0; itRow < nbKeys; ++itRow)
	  {
	    std::getline(inFile,currentLine,eolChar);
	    std::istringstream issCurrentLine(currentLine);
	    for(unsigned int itCol = 0;itCol < nbLabels; ++itCol)
	      {
		std::pair<std::string,unsigned int> key = std::make_pair(vectorOfKey[itRow],vectorLabels[itCol]);
		std::getline(issCurrentLine,currentValue,separatorChar);
		mapOfKey[key] = static_cast<double> (std::atof(currentValue.c_str()));
	      }
	  }

	return EXIT_SUCCESS;
      }

  }

  void DoExecute()
  {
    FloatVectorImageListType::Pointer imageList = GetParameterImageList("il");
    FloatVectorImageListType::Pointer refList = GetParameterImageList("ref");

    if(imageList->Size() < 2 )
      {
  	itkExceptionMacro("This vote method requires at least two voters");
      }

    // Convert tr in unordered map
    std::string fileName = GetParameterString("tr");
    //MatrixType transitionMatrix;
    //LabelToIndicesType labelsToIndices;
    //KeyToIndicesType keyToIndices;
    LabelVectorType vectorLabels;
    MapOfKeysType mapOfKey;
    CSVTransitionMatrixFileReader(fileName,mapOfKey,vectorLabels);
    imageList->GetNthElement(0)->UpdateOutputInformation();
    m_ImageList = ImageListType::New();
    m_Concatener = ListConcatenerFilterType::New();
    for(unsigned int i=0; i<imageList->Size(); ++i)
      {
  	FloatVectorImageType::Pointer image = imageList->GetNthElement(i);
  	image->UpdateOutputInformation();
  	ExtractROIFilterType::Pointer converter = ExtractROIFilterType::New();
  	converter->SetInput(image);
  	converter->SetChannel(1);
  	converter->UpdateOutputInformation();
  	m_ExtractorList->PushBack(converter);
  	m_ImageList->PushBack(converter->GetOutput());
      }
    m_Concatener->SetInput(m_ImageList);
    std::cout << "Ref concat" << std::endl;
    m_RefList = ImageListType::New();
    m_ConcRef = ListConcatenerFilterType::New();
    for(unsigned int i=0; i<refList->Size();++i)
      {
	FloatVectorImageType::Pointer imRef = refList->GetNthElement(i);
	imRef->UpdateOutputInformation();
	ExtractROIFilterType::Pointer conv = ExtractROIFilterType::New();
	conv->SetInput(imRef);
	conv->SetChannel(1);
	conv->UpdateOutputInformation();
	m_ExtractorList->PushBack(conv);
	m_RefList->PushBack(conv->GetOutput());
      }
    m_ConcRef->SetInput(m_RefList);
    // Filter
    std::cout << "Filter" << std::endl;
    filter = FilterType::New();
    if(GetParameterString("mode") == "classes")
      {
	filter->SetFlagGuide(false);
	filter->SetFlagGuideN(false);
      }
    else if (GetParameterString("mode") == "voters")
      {
	std::cout << "Mode voters" << std::endl;
	filter->SetFlagGuide(true);
	filter->SetFlagGuideN(false);
      }
    else if(GetParameterString("mode") == "hvoters")
      {
	filter->SetFlagGuide(true);
	filter->SetFlagGuideN(true);
	filter->SetHVotersMarge(GetParameterInt("hvoters.marge"));
      }
      
    if (HasValue("margin"))
      {
	if(GetParameterInt("margin") < int(imageList->Size()))
	  {
	    filter->SetMargin(GetParameterInt("margin"));
	  }
	else
	  {
	    filter->SetMargin(0);
	  }
      }

    filter->SetInput(0, m_Concatener->GetOutput());
    filter->SetInput(1,m_ConcRef->GetOutput());

    filter->SetMapOfKey(mapOfKey);
    filter->SetVectorLabel(vectorLabels);
    filter->UpdateOutputInformation();
    SetParameterOutputImage("out",filter->GetOutput());
  }
  ListConcatenerFilterType::Pointer  m_Concatener;
  ExtractROIFilterListType::Pointer  m_ExtractorList;
  ListConcatenerFilterType::Pointer  m_ConcRef;
  ImageListType::Pointer        m_ImageList;
  ImageListType::Pointer        m_RefList;
  FilterType::Pointer filter;
}; //end of class VoteTransition
}//end namespace Wrapper
}//end namespace otb
OTB_APPLICATION_EXPORT(otb::Wrapper::VoteOCSTransition)
